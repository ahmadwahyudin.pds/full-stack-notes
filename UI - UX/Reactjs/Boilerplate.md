# Boilerplate React Js 

## Folder Structure 

```
.
└── root/
    ├── public/
    │   ├── css
    │   ├── js
    │   └── images
    └── src/
        ├── features/
        │   ├── login/
        │   │   ├── index.tsx
        │   │   ├── Login.tsx
        │   │   └── Login.css
        │   └── signup/
        │       ├── index.tsx 
        │       ├── Signup.tsx 
        │       └── Signup.css
        ├── components/
        │   ├── select/
        │   │   ├── index.tsx
        │   │   ├── select.component.tsx
        │   │   └── select.css
        │   └── inputtext/
        │       ├── index.tsx
        │       ├── inputtext.component.tsx
        │       └── inputtext.css
        ├── pages/
        │   └── introduction/
        │       ├── index.tsx
        │       ├── introduction.page.tsx
        │       └── introduction.css
        ├── contexts
        ├── hooks 
        └── fetch 
```

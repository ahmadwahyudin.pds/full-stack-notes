# Absoulute Imports 

Absolute Import has been launched in create react app v3 . 
Let’s go over a few benefits of absolute import if you are still not convinced else skip this part.

```
tsconfig.json
{
  "compilerOptions": {
    "baseUrl": "src",
	"paths": {
		"@tests/*": ["src/tests/*"]
	}
  },
  "include": ["src"] 
}

```

```ts

vite.config.ts

import { defineConfig } from "vite";
import react from "@vitejs/plugin-react-swc";

// https://vitejs.dev/config/
export default defineConfig({
	plugins: [react()],
	server: {
		port: 3001,
	},
	resolve: {
		alias: [
			{
				find: "@tests",
				replacement: "/src/tests",
			},
		],
	},
});
``` 